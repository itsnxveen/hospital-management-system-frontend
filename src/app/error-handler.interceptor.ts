import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable , catchError, throwError} from 'rxjs';
import { JwtClientService } from './jwt-client.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(private jwtservice:JwtClientService,private toastr:ToastrService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    return next.handle(request).pipe(
      catchError(errordata =>{
       if(errordata.status===400){
         this.toastr.error(errordata.error)
       }
       
      return throwError(errordata);
      
      })
  );
  }
}
