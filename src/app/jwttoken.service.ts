import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';

import { Injectable, Injector } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { JwtClientService } from './jwt-client.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',

})

export class JwttokenService implements HttpInterceptor {

  constructor(private jwtService:JwtClientService,private toastr:ToastrService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    
    const accessToken = req.clone({
      setHeaders:{
        
        Authorization: 'Bearer ' + this.jwtService.Token,
      
      },
   
    });
    return next.handle(accessToken)
  }

}


