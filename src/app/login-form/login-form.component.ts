import { Component } from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { JwtClientService } from '../jwt-client.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent {
  username!: any;
  

  constructor(private jwtService:JwtClientService ,private router:Router,private toastr:ToastrService){}

  login=new FormGroup({
    userName:new FormControl("",Validators.required),
    password:new FormControl("",Validators.required)  
  })
  
  name!:any;
  GetData(){
    console.log(this.login.value)
    this.jwtService.userlogin(this.login.value).subscribe(
    (response:any) => {
      this.toastr.success('Login Successfull');
        console.log(response);

       localStorage.setItem('accesstoken',response);

       this.name=this.login.value.userName;

       localStorage.setItem('name',this.name);
       
       this.router.navigate(['/menu'])
       
     }
     )
  }

  get vname(){
    return this.login.get("userName");
  }
  get vpwd(){
    return this.login.get("password");
  }
 
  
}
