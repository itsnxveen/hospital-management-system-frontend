import { Component } from '@angular/core';
import { JwtClientService } from '../jwt-client.service';

@Component({
  selector: 'app-healthcareproviders',
  templateUrl: './healthcareproviders.component.html',
  styleUrls: ['./healthcareproviders.component.css']
})
export class HealthcareprovidersComponent {

  totalLength:any;
  page:number=1;

 healthcls!:healthVo;

 constructor(private jwtservice:JwtClientService){}

  hospital=[{id:1,name:"Apollo"},{id:2,name:"kauvery"}];
  defaultSelect=0;

  docDetails:any;
  hospitalName!:string;

  getHospitals(data:any){
    this.hospitalName=data.target.value;
  }
  hName!:any;
  onSubmit(){

    //  this.healthcls.hospitalName=this.hospitalName;
    this.healthcls=new healthVo(this.hospitalName)

    this.jwtservice.getDoctorsByHospital(this.healthcls).subscribe(
      (response) => {
      console.log(response);
      this.docDetails=response;
      this.hName=this.docDetails.hospitalName
      })
  }

}
export class healthVo{

  hospitalName!:any;
  
  constructor(hospitalName:any){
    this.hospitalName=hospitalName;
  }
}