import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthcareprovidersComponent } from './healthcareproviders.component';

describe('HealthcareprovidersComponent', () => {
  let component: HealthcareprovidersComponent;
  let fixture: ComponentFixture<HealthcareprovidersComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HealthcareprovidersComponent]
    });
    fixture = TestBed.createComponent(HealthcareprovidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
