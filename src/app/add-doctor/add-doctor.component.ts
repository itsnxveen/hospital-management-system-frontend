import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { JwtClientService } from '../jwt-client.service';

@Component({
  selector: 'app-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.css']
})
export class AddDoctorComponent {

   doctorForm: FormGroup;

  constructor(private formBuilder: FormBuilder ,private jwtService:JwtClientService , private toastr: ToastrService){
       
  this.doctorForm = this.formBuilder.group({
      doctorName: ['', Validators.required],
      department: ['', Validators.required],
      location:['',Validators.required],
      hospitalName:['',Validators.required],
      consulationFees:['',Validators.required]
    });

   }
  //  show(){
  //    this.toastr.success('Doctor added');
  //  }
   
addDoctor() {
  if (this.doctorForm.valid) {
    // Call your service to add a new doctor to the database
    // For example: this.appointmentService.addDoctor(this.doctorForm.value).subscribe(...)
    // After successfully adding the doctor, you might want to reload the list of doctors or perform other actions
    this.jwtService.adddoctor(this.doctorForm.value).subscribe(
      (response)=>{
        let data=response
        if(data)
        {
        this.toastr.success('Doctor added');
        console.log(response)
        }
      },
      (error)=>{
        this.toastr.error('error occured');
      }

    )   
  } else {
    this.toastr.error('Please fill out all fields.');
  }
}
}
