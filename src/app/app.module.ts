import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SecurityComponent } from './security/security.component';
import { WelcomeContentComponent } from './welcome-content/welcome-content.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ContentComponent } from './content/content.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'; 
import { JwtClientService } from './jwt-client.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RegisterFormComponent } from './register-form/register-form.component';
import { BookingComponentComponent } from './booking-component/booking-component.component';
import { HealthcareprovidersComponent } from './healthcareproviders/healthcareproviders.component';
import { JwttokenService } from './jwttoken.service';
import { CookieService } from 'ngx-cookie-service';
import { ToastrModule } from 'ngx-toastr';
import { ProvidersLoginComponent } from './providers-login/providers-login.component';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';
import { ErrorHandlerInterceptor } from './error-handler.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SecurityComponent,
    WelcomeContentComponent,
    LoginFormComponent,
    ContentComponent,
    RegisterFormComponent,
    BookingComponentComponent,
    HealthcareprovidersComponent,
    ProvidersLoginComponent,
    AddDoctorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({

    })
  ],
  
  providers: [
    JwtClientService,
    JwttokenService,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwttokenService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
