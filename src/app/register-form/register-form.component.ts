import { Component } from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { JwtClientService } from '../jwt-client.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent {

 

  constructor(private jwtService: JwtClientService ){}

  register=new FormGroup({
    userName:new FormControl("",Validators.required),
    password:new FormControl("",Validators.required),
    email:new FormControl("",Validators.required),
    role:new FormControl()
  })

  addUser(){
     this.jwtService.userRegister(this.register.value).subscribe(
      (response) => {
        console.log(response);
        alert("Registeration Successfull")
     },
       (error) => {
        alert("Registeration failed")
       console.log(error);
       }
     )
  }

  get vname(){
    return this.register.get("userName");
  }
  get vpwd(){
    return this.register.get("password");
  }
  get vemail(){
    return this.register.get("email");
  }
  get vrole(){
    return this.register.get('role');
  }
}
