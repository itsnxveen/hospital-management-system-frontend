import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppointmentServiceService {

  BACKEND_API_URL="http://localhost:8080" // Replace this with your actual backend API URL

  constructor(private http: HttpClient) { }

  // getPendingAppointments(hospitalName:any){

  //   return this.http.post(this.BACKEND_API_URL,'/getByHospitalName')

  // }

  acceptAppointment(appointmentId: number): Observable<any> {
    // const url = `${this.apiUrl}/appointments/${appointmentId}/accept`; // Modify the endpoint as per your API
    return this.http.put(this.BACKEND_API_URL+'/addDoctor',appointmentId)
  }

  // adddoctor(adddoctordata:any){
     
  //     return this.http.post(this.BACKEND_API_URL+'/addDoctor',adddoctordata)
  // }


 
}
