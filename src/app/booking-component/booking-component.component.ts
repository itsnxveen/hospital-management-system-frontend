import { Component,OnInit } from '@angular/core';
import { JwtClientService } from '../jwt-client.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-booking-component',
  templateUrl: './booking-component.component.html',
  styleUrls: ['./booking-component.component.css']
})
export class BookingComponentComponent implements OnInit {

  totalLength:any;
  page:number=1;

  appointments:any;

  constructor(private jwtService:JwtClientService, private toastr:ToastrService  ){}

  ngOnInit(){

    this.jwtService.getAppointments().subscribe(
      (data)=>{
        this.appointments=data;
        console.log(data);
      }
    );

  }

id:any;
  
  cancelBooking(appoint:any){

    this.id=appoint.id
    alert("are you sure want cancel the Appointment")
    this.jwtService.cancelAppointment(this.id).subscribe(
      (response)=>{
         this.toastr.success("Appointment Cancelled")
      },
      (error)=>{
        this.toastr.error("Appointment Cancelled")
      }
    );

  }


}
