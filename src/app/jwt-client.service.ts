import { Injectable } from '@angular/core';
import{ HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class JwtClientService {

  PATH_OF_API ="http://localhost:8080"

  constructor(private httpclient: HttpClient) { }

  public userlogin(loginData:any){

      return this.httpclient.post(this.PATH_OF_API+'/genToken',loginData,{responseType:'text' as 'json'});
  }

  public userRegister(registerData:any):Observable<object>{

    return this.httpclient.post(this.PATH_OF_API+'/registration',registerData,{responseType:'text' as 'json'});
  }

  public getDoctors(doctors:any){
   
    return this.httpclient.post(this.PATH_OF_API+'/search',doctors)

  }

  public appointmentBooking(bookingData:any){

    return this.httpclient.post(this.PATH_OF_API+'/booking',bookingData)
  }

  adddoctor(adddoctordata:any){
     
    return this.httpclient.post(this.PATH_OF_API+'/addDoctor',adddoctordata)
  }

  public getAppointments(){
     return  this.httpclient.get(this.PATH_OF_API+'/getAppointments')
  }
  
   public getDoctorsByHospital(doctorsData:any){

    return this.httpclient.post(this.PATH_OF_API+'/getByHospital',doctorsData)
   }

   public getByemail(email:any){

    return this.httpclient.post(this.PATH_OF_API+'/getByEmail',email)
   }

   public cancelAppointment(id:any){
    return this.httpclient.delete(this.PATH_OF_API+'/deleteById/'+id)
   }

   public getPendingAppointments(hospitalName:any){

    return this.httpclient.post(this.PATH_OF_API+'/getByHospitalName',hospitalName)
    
  }

   public acceptAppointment(id:any,updateDetails:any){

      return this.httpclient.put(this.PATH_OF_API+'/updateStatus/'+id,updateDetails)
   }

   public cancelAppointmentByAdmin(id:number){
    return this.httpclient.delete(this.PATH_OF_API+'/deleteByAdmin/'+id)
   }

   private hospitalName: any;

   public setHospitalName(hospitalName: string) {
     this.hospitalName = hospitalName;
   }
 
   public getHospitalName(): string | null | undefined {
     
     
     return this.hospitalName;
   }
 
  jwtToken:any='';
  get Token() {

    if (localStorage.getItem('accesstoken')) {
      this.jwtToken = localStorage.getItem('accesstoken');
    }
    return this.jwtToken;

  }

}
