import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppointmentServiceService } from 'src/app/appointment-service.service';
import { JwtClientService } from 'src/app/jwt-client.service';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap/pagination/pagination';


@Component({
  selector: 'app-provider-home',
  templateUrl: './provider-home.component.html',
  styleUrls: ['./provider-home.component.css']
})
export class ProviderHomeComponent implements OnInit {
  
  public page =10;
  public pageSize=10;

  appointmentDetails=new details();
 
  pendingAppointments!: any;

  constructor(private jwtService: JwtClientService, private toastr: ToastrService
    ,private appointmentService:AppointmentServiceService) {
    
  }

  hospitalName!:any;

  ngOnInit(): void {

    this.loadPendingAppointments()
    

  }
  
   
  loadPendingAppointments() {
    
     this.appointmentDetails.hospitalName=localStorage.getItem('hosName')

    this.jwtService.getPendingAppointments(this.appointmentDetails).subscribe(
      (response) => {
        this.pendingAppointments = response;
        console.log(this.hospitalName)
      },
      (error) => {
        console.log(error);
      }
    );
   
  }


  acceptAppointment(appointmentId: number) {

    this.appointmentDetails.status="Booked";

    this.jwtService.acceptAppointment(appointmentId,this.appointmentDetails).subscribe(
      (response) => {
        this.toastr.success('Appointment Accepted');
         
      },
      (error) => {
        this.toastr.error('Appointment not Accepted');
      }
    );
  }

  id!:number;
  deleteAppointment(appointmentId:number){
    
    this.id=appointmentId;

    this.jwtService.cancelAppointmentByAdmin(this.id).subscribe(

      (response) => {
        this.toastr.error('Appointment cancelled');
      },
      (error) => {
        this.toastr.error('Appointment cancelled');
      }
    )

  }

  
}

export class details{

  hospitalName:any;
  status:any;

}
