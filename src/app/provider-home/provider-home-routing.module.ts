import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProviderHomeComponent } from './provider-home/provider-home.component';
import { AddDoctorComponent } from '../add-doctor/add-doctor.component';

const routes: Routes = [{
  path:'',component:ProviderHomeComponent
},
{
  path:'add-doc',component:AddDoctorComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderHomeRoutingModule { }
