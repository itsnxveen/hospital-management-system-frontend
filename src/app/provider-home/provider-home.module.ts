import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderHomeRoutingModule } from './provider-home-routing.module';
import { ProviderHomeComponent } from './provider-home/provider-home.component';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ProviderHomeComponent
  ],
  imports: [
    CommonModule,
    ProviderHomeRoutingModule,ReactiveFormsModule
  ]
})
export class ProviderHomeModule { }
