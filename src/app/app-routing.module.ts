import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { WelcomeContentComponent } from './welcome-content/welcome-content.component';
import { HomeComponent } from './home/home/home.component';
import { BookingComponentComponent } from './booking-component/booking-component.component';
import { HealthcareprovidersComponent } from './healthcareproviders/healthcareproviders.component';
import { ProvidersLoginComponent } from './providers-login/providers-login.component';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';

const routes: Routes = [
  {path:'',component :LoginFormComponent},
  {path:'providers',component:ProvidersLoginComponent},
  {path:'register-form',component:RegisterFormComponent},
  {path:'welcome-content',component:WelcomeContentComponent},
  {path:'booking-component',component:BookingComponentComponent},
  {path:'providers',component:HealthcareprovidersComponent},
  {path:'home',component:HomeComponent},
  {path:'add-doc',component:AddDoctorComponent},
  {path:'menu',loadChildren:()=>
    import('./home/home.module').then((m) => m.HomeModule)
  },
  {path:'provider',loadChildren:()=>
    import('./provider-home/provider-home.module').then((m) => m.ProviderHomeModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
