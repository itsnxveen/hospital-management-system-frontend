import { Component } from '@angular/core';
import { DoctorInterface } from '../doctor-interface';
import { JwtClientService } from '../jwt-client.service';
import {FormControl, FormGroup,} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-welcome-content',
  templateUrl: './welcome-content.component.html',
  styleUrls: ['./welcome-content.component.css']
})
export class WelcomeContentComponent {

  totalLength:any;
  page:number=1;
  
   doctorDetails:any;

   bookingcls= new bookingVo();

  constructor(private jwtService:JwtClientService,private toastr:ToastrService){}

   locate!:string;
   depart!:string;

  location=[{id:1,name:"chennai"},{id:2,name:"kochin"},{id:3,name:"bangalore"}];
  defaultSelect=0;

  department=[{id:1,name:"dentist"},{id:2,name:"homeopath"},{id:3,name:"dermatologist"},{id:4,name:"general physician"}];
  default=0;

  timeslot=[{id:1,name:"10.00-11.00"},{id:2,name:"11.00-12.00"},{id:3,name:"12.00-1.00"}]

 
  doctorName!:any;
  hospitalName!:any;
  consulationFees!:any;
  initialize(doc:any){
        this.doctorName= doc.doctorName
        this.hospitalName= doc.hospitalName
        this.consulationFees=doc.consulationFees 
  }
   

  booking=new FormGroup({
        patientName:new FormControl(""),
        age:new FormControl(""),
        email:new FormControl(""),
        dateAndTime:new FormControl(""),
        time:new FormControl(""),

  });
  dropdefault=0;
  // status!:any;

  getLocation(event:any){
 
     this.locate=event.target.value;
  }

  getDept(event:any){
     this.depart=event.target.value;
  }

  doctors!:DoctorInterface;
  
  show(){
    
  }

  onSubmit(){
          console.log(this.locate)
          console.log(this.depart)
          this.doctors= new DoctorInterface(this.locate,this.depart);
   
          this.jwtService.getDoctors(this.doctors).subscribe(
          (response) => {
         
          console.log(response);
          this.doctorDetails=response;
          console.log(this.doctorDetails); 
          }
     )
  }
  status:string='Requested';

  bookAppointment(){
          this.bookingcls.age=this.booking.value.age;
          this.bookingcls.patientName=this.booking.value.patientName;
          this.bookingcls.email=this.booking.value.email;
          this.bookingcls.dateAndTime=this.booking.value.dateAndTime;
          this.bookingcls.time=this.booking.value.time;

          this.bookingcls.doctorName=this.doctorName;
          this.bookingcls.hospitalName=this.hospitalName;
          this.bookingcls.consulationFees=this.consulationFees;
          this.bookingcls.status=this.status;
          console.log(this.bookingcls)

          this.jwtService.appointmentBooking(this.bookingcls).subscribe(
          (response)=>{

           this.status="Requested";
         
          // this.gmail=this.bookingcls.email;
          // this.bookComp.getEmail(this.gmail);
          this.toastr.success("Your Appointment booked successfully");
          console.log(response);
          },
         (error) => {
         alert("booking Failed")
         console.log(error);
         }
    )
  }
  

}

export class bookingVo{

  doctorName!:any;
  hospitalName!:any;
  consulationFees!:any;
  patientName!:any;
  age!:any;
  email!:any;
  dateAndTime!:any;
  time!:any;
  status!:any;
}

