import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { WelcomeContentComponent } from '../welcome-content/welcome-content.component';
import { BookingComponentComponent } from '../booking-component/booking-component.component';
import { HealthcareprovidersComponent } from '../healthcareproviders/healthcareproviders.component';

const routes: Routes = [
  {
    path:'', component:HomeComponent
    ,children:[
     {path:'',component:WelcomeContentComponent} ,
     {path:'booking-component',component:BookingComponentComponent},
     {path:'providers',component:HealthcareprovidersComponent}
    ],

  },
  {path:'booking-component',component:BookingComponentComponent},
  {path:'providers',component:HealthcareprovidersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
