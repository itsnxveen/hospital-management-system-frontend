import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidersLoginComponent } from './providers-login.component';

describe('ProvidersLoginComponent', () => {
  let component: ProvidersLoginComponent;
  let fixture: ComponentFixture<ProvidersLoginComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProvidersLoginComponent]
    });
    fixture = TestBed.createComponent(ProvidersLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
