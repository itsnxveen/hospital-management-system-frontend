import { Component } from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { JwtClientService } from '../jwt-client.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-providers-login',
  templateUrl: './providers-login.component.html',
  styleUrls: ['./providers-login.component.css']
})
export class ProvidersLoginComponent {

  hospitalName!:any;

  constructor(private jwtService:JwtClientService ,private router:Router,private toastr:ToastrService){}

  login=new FormGroup({
    userName:new FormControl("",Validators.required),
    password:new FormControl("",Validators.required)  
  })

  username:any;


  GetData(){
    console.log(this.login.value)
    this.jwtService.userlogin(this.login.value).subscribe(
    (response:any) => {
        this.toastr.success('Login Successfull');
        console.log(response);

        localStorage.setItem('accesstoken',response);
    
        this.hospitalName=this.login.value.userName
      
        this.jwtService.setHospitalName(this.hospitalName);

        this.username=this.login.value.userName

        localStorage.setItem('hosName',this.username)
        
        console.log(localStorage.getItem('hosName'));

        this.router.navigate(['/provider'])
     },
       (error) => {
        this.toastr.error('Login Failed');
        console.log(error);
       }
     )
  }

  get vname(){
    return this.login.get("userName");
  }
  get vpwd(){
    return this.login.get("password");
  }

  
}
